<p align="center">
  <img src="./assets/concept.jpg">
</p>

<p align="center">
  <img src="./assets/ScreenShots.png">
</p>

## **Installation**

```
git clone https://github.com/BREDFactory/Tribu.git
cd Tribu
```

Make sure you have installed [XCode](https://developer.apple.com/xcode/) and then open the project using it.
You can now **build** the project and **run it** on your device or the simulator.

You also need to create a Micorsoft Azure Face API key to be able to use the face recognition feature.

Then, put your API key inside the `APIFaceRecognition.swift` config:

```js
let APIKey = "" // Ocp-Apim-Subscription-Key
```

**Note**   
This app is still in status Work in progress.