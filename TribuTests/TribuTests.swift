//
//  TribuTests.swift
//  TribuTests
//
//  Created by Rémi Doreau on 31/01/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import XCTest
import Foundation
import UIKit

import Alamofire
import SwiftyJSON

@testable import Tribu

let APIKey = "" // Ocp-Apim-Subscription-Key
let Region = "francecentral"
let FindSimilarsUrl = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/identify"
let DetectUrl = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/detect"

let AddFaceToGroup = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/persongroups/"
let AddPersonToGroup = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/persongroups/"
let CreateGroup = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/persongroups/"
let Train = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/persongroups/"
let Identify = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/identify"

class TribuTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    /**
     *  PersonGroup
     */
    
    /*********************************************************************************************************************/
    
    // Function used to create a new group of persons
    func createGroup(groupName: String, name: String /* , userData: String */ ) -> Void {
        let asyncExpectation = expectation(description: "createFunction test")
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]
        
        let parameters: [String: String] = [
            "name" : name,
//            "userData" : userData,
        ]
        
        Alamofire.request("\(CreateGroup)\(groupName)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                    asyncExpectation.fulfill()
                    XCTAssertEqual(response.response?.statusCode, 200, "We should have response code 200.")
        }
        waitForExpectations(timeout: 5)
    }
    
    // Function used to add a new person to an existing group
    func addPersonToGroup(personGroupId: String, name: String /* , userData: String */  ) -> Void {
        let asyncExpectation = expectation(description: "addPersonToGroup test")
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]
        
        let parameters: [String: String] = [
            "name" : name,
            // "userData" : userData,
        ]
        
        Alamofire.request("\(AddPersonToGroup)\(personGroupId)/persons", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                print(response)
                asyncExpectation.fulfill()
                XCTAssertEqual(response.response?.statusCode, 200, "We should have response code 200.")
        }
        waitForExpectations(timeout: 5)
    }
    
    
    // Function used to add a new person to an existing group
    func addFaceToPerson(imageName: String, personGroupId: String, personId: String ) -> Void {
        let asyncExpectation = expectation(description: "detectFaces test")
        let bundle = Bundle(identifier: "fr.bredfactory.tribu")
        let imageTest = UIImage(named: imageName, in: bundle, compatibleWith: nil)
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/octet-stream",
            "Ocp-Apim-Subscription-Key": APIKey
        ]
        
        let imageData = UIImageJPEGRepresentation(imageTest!, 0.7)
        
        Alamofire.upload(imageData!, to: "\(AddFaceToGroup)\(personGroupId)/persons/\(personId)/persistedFaces", method: .post, headers: headers)
            .responseJSON { response in
                asyncExpectation.fulfill()
                let result = JSON(response.result.value)
                print(result)
                XCTAssertEqual(response.response?.statusCode, 200, "We should have response code 200.")
        }
        waitForExpectations(timeout: 5)
    }
    
    // Function used to train the model
    func train(personGroupId: String ) -> Void {
        let asyncExpectation = expectation(description: "train test")
        let headers: HTTPHeaders = [
            "Ocp-Apim-Subscription-Key": APIKey
        ]
        
        Alamofire.request("\(Train)\(personGroupId)/train", method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                print(response)
                asyncExpectation.fulfill()
                XCTAssertEqual(response.response?.statusCode, 202, "We should have response code 202.")
        }
        waitForExpectations(timeout: 5)
    }
    
    /*********************************************************************************************************************/
    
    /**
     *  Face
     */
    
    /*********************************************************************************************************************/
    
    // Detect human faces in an image, return face rectangles, and optionally with faceIds, landmarks, and attributes.
    func detectFaces(imageName: String, completion: @escaping (String) -> ()) {
        let bundle = Bundle(identifier: "fr.bredfactory.tribu")
        let imageTest = UIImage(named: imageName, in: bundle, compatibleWith: nil)
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/octet-stream",
            "Ocp-Apim-Subscription-Key": APIKey
        ]
        
        let imageData = UIImageJPEGRepresentation(imageTest!, 0.7)

        Alamofire.upload(imageData!, to: DetectUrl, method: .post, headers: headers)
            .responseJSON { response in
                let result = JSON(response.result.value)
                completion(result[0]["faceId"].string!)
        }
         waitForExpectations(timeout: 5)
    }

    // Function used to identify a person given a personGroupId
    func identifyPerson(faceIds: [String], personGroupId: String ) -> Void {
        let asyncExpectation = expectation(description: "identifyPerson test")
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let parameters: [String: Any] = [
            "faceIds" : faceIds,
            "personGroupId": personGroupId
        ]

        Alamofire.request(Identify, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                let result = JSON(response.result.value)
                print(result)
                XCTAssertEqual(response.response?.statusCode, 200, "We should have response code 200.")
        }
        waitForExpectations(timeout: 8)
    }
    
    /*********************************************************************************************************************/
    
    
    
    func testExample() {

    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
