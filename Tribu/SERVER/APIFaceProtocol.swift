//
//  APIFaceProtocol.swift
//  Tribu
//
//  Created by Rémi Doreau on 07/03/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import Foundation
import UIKit

import SwiftyJSON

protocol APIFaceProtocol {
    
    // Create a new person group with specified personGroupId, name, and user-provided userData.
    func createGroup(groupName: String, name: String, completion: @escaping (Int) -> () /* , userData: String */)
    
    // Create a new person in a specified person group.
    func addPersonToGroup(personGroupId: String, name: String, completion: @escaping (String) -> () /* , userData: String */  )
    
    // Add a face image to a person into a person group for face identification or verification.
    func addFaceToPerson(image: UIImage, personGroupId: String, personId: String, completion: @escaping (String) -> () )
    
    // Submit a person group training task. Training is a crucial step that only a trained person group can be used by Face - Identify.
    func train(personGroupId: String, completion: @escaping (Int) -> () )
    
    // Detect human faces in an image, return face rectangles, and optionally with faceIds, landmarks, and attributes.
    func detectFaces(image: UIImage, completion: @escaping (String) -> ())
    
    // 1-to-many identification to find the closest matches of the specific query person face from a person group or large person group.
    func identifyPerson(faceIds: [String], personGroupId: String, completion: @escaping (JSON) -> () )
    
    // Retrieve a person's name and userData, and the persisted faceIds representing the registered person face image.
    func retrievePerson(personId: String, personGroupId: String, completion: @escaping (JSON) -> () )
}
