//
//  APIFaceRecognition.swift
//  Tribu
//
//  Created by Rémi Doreau on 07/03/2019.
//  Copyright © 2019 BRED. All rights reserved.
//


/**
 *  Before using the class methods to register / identify,  you need to create a new group :
 *
 *      createGroup(groupName: "test group name", name: "test name") { createStatus in
 *          if (createStatus == 202) {
 *              print("The group was successfully created !")
 *          }
 *      }
 */

/**
 *  Example of use to register a new person :
 *
 *  addPersonToGroup(personGroupId: "test", name: "testName") { personIdResult in
 *      let personId = personIdResult
 *              addFaceToPerson(image: UIImage(named: "test"), personGroupId: "test", personId: personId) { persistedFaceIdResult in
 *                    let persistedFaceId = persistedFaceIdResult
 *                       train(personGroupId: "test") { trainStatus in
 *                           if (trainStatus == 202) {
 *                               print("Success")
 *                           }
 *                       }
 *               }
 *  }
 */

/**
 *  Example of use to identify a person :
 *
 *  detectFaces(image: UIImage(named: "test")) { faceIdResult in
 *      let faceId = faceIdResult
 *              identifyPerson(faceIds: [faceId], personGroupId: "test") { identifiedPersonResult in
 *                    let identifiedPerson = identifiedPersonResult
 *                    let personId = identifiedPerson[0]["candidates"][0]["personId"]
 *                       retreivePerson(personId: personId, personGroupId: "test") { trainStatus in
 *                           if (trainStatus == 202) {
 *                               print("Success")
 *                           }
 *                       }
 *               }
 *  }
 */


import Foundation
import UIKit
import SwiftyJSON

import Alamofire

let APIKey = "" // Ocp-Apim-Subscription-Key
let Region = "francecentral"

// API face endpoints
let DetectUrl = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/detect"
let APIPersonGroups = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/persongroups/"
let Identify = "https://\(Region).api.cognitive.microsoft.com/face/v1.0/identify"

class APIFaceRecognition: APIFaceProtocol {

    /**
     *  PersonGroup
     */

    // Function used to create a new group of persons
    
    func createGroup(groupName: String, name: String, completion: @escaping (Int) -> () /* , userData: String */ ) -> Void {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let parameters: [String: String] = [
            "name" : name,
            //            "userData" : userData,
        ]

        Alamofire.request("\(APIPersonGroups)\(groupName)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                completion((response.response?.statusCode)!)
        }
    }

    // Function used to add a new person to an existing group
    
    func addPersonToGroup(personGroupId: String, name: String, completion: @escaping (String) -> () /* , userData: String */  ) -> Void {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let parameters: [String: String] = [
            "name" : name,
            // "userData" : userData,
        ]

        Alamofire.request("\(APIPersonGroups)\(personGroupId)/persons", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                let result = JSON(response.result.value ?? "")
                completion(result["personId"].string!)
        }
    }


    // Function used to add a new person to an existing group
    
    func addFaceToPerson(image: UIImage, personGroupId: String, personId: String, completion: @escaping (String) -> () ) -> Void {
//        let bundle = Bundle(identifier: "fr.bredfactory.tribu")
//        let imageTest = UIImage(named: imageName, in: bundle, compatibleWith: nil)

        let headers: HTTPHeaders = [
            "Content-Type": "application/octet-stream",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let imageData = UIImageJPEGRepresentation(image, 0.7)

        Alamofire.upload(imageData!, to: "\(APIPersonGroups)\(personGroupId)/persons/\(personId)/persistedFaces", method: .post, headers: headers)
            .responseJSON { response in
                let result = JSON(response.result.value ?? "")
                completion(result["persistedFaceId"].string!)
        }
    }

    // Function used to train the model
    
    func train(personGroupId: String, completion: @escaping (Int) -> () ) -> Void {
        let headers: HTTPHeaders = [
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        Alamofire.request("\(APIPersonGroups)\(personGroupId)/train", method: .post, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                completion((response.response?.statusCode)!)
        }
    }

    /**
     *  Face
     */

    // Detect human faces in an image, return face rectangles, and optionally with faceIds, landmarks, and attributes.
    
    func detectFaces(image: UIImage, completion: @escaping (String) -> ()) -> Void {
//        let bundle = Bundle(identifier: "fr.bredfactory.tribu")
//        let imageTest = UIImage(named: imageName, in: bundle, compatibleWith: nil)

        let headers: HTTPHeaders = [
            "Content-Type": "application/octet-stream",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let imageData = UIImageJPEGRepresentation(image, 0.7)

        Alamofire.upload(imageData!, to: DetectUrl, method: .post, headers: headers)
            .responseJSON { response in
                let result = JSON(response.result.value ?? "")
                completion(result[0]["faceId"].string!)
        }
    }

    // Function used to identify a person given a personGroupId
    
    func identifyPerson(faceIds: [String], personGroupId: String, completion: @escaping (JSON) -> () ) -> Void {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]

        let parameters: [String: Any] = [
            "faceIds" : faceIds,
            "personGroupId": personGroupId
        ]

        Alamofire.request(Identify, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                let result = JSON(response.result.value ?? "")
                completion(result)
        }
    }
    
    // Function used to get detected user data
    
    func retrievePerson(personId: String, personGroupId: String, completion: @escaping (JSON) -> () ) -> Void {
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Ocp-Apim-Subscription-Key": APIKey
        ]
        
        Alamofire.request("\(APIPersonGroups)\(personGroupId)/persons/\(personId)", method: .get, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                let result = JSON(response.result.value ?? "")
                completion(result)
        }
    }
}

