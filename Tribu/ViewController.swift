//
//  ViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 31/01/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    let apiface = APIFaceRecognition()
    
    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var lastConnectionDate: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var photoButton: UIImageView!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var forgottenPinButton: UIButton!
    
    let captureSession = AVCaptureSession()
    // If we find a device we'll store it here for later use
    var captureDevice : AVCaptureDevice?
    var videoPreviewLayer:AVCaptureVideoPreviewLayer?
    var pictureFrameView:UIView?
    var stillImageOutput:AVCaptureStillImageOutput = AVCaptureStillImageOutput()
    var image:UIImage                   =   UIImage()
    var imageData:NSData                =   NSData()
    var idCardScanning:Bool             =   false
    
    var recognized:Bool                 =   false
    
    private let supportedCodeTypes = [AVMetadataObject.ObjectType.upce,
                                      AVMetadataObject.ObjectType.code39,
                                      AVMetadataObject.ObjectType.code39Mod43,
                                      AVMetadataObject.ObjectType.code93,
                                      AVMetadataObject.ObjectType.code128,
                                      AVMetadataObject.ObjectType.ean8,
                                      AVMetadataObject.ObjectType.ean13,
                                      AVMetadataObject.ObjectType.aztec,
                                      AVMetadataObject.ObjectType.pdf417,
                                      AVMetadataObject.ObjectType.itf14,
                                      AVMetadataObject.ObjectType.dataMatrix,
                                      AVMetadataObject.ObjectType.interleaved2of5,
                                      AVMetadataObject.ObjectType.qr,
                                      AVMetadataObject.ObjectType.face]
    
    @IBAction func takePhoto(_ sender: Any) {
     if !recognized {
         beginScan()}
     else{
           let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "HomeContainer")
           self.navigationController?.pushViewController(vc, animated: true)
     }
    }
    func beginScan(){
        
        if let videoConnection = stillImageOutput.connection(with: AVMediaType.video) {
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                self.imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(imageDataSampleBuffer!) as! NSData
                
                self.image   =   UIImage(data: self.imageData as Data)!
                self.image   =   self.resize(image: self.image)
                self.captureSession.stopRunning()

                self.identifyPerson(personGroupId: "chief", image: self.image)
            }
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lastConnectionDate.isHidden = false
        
        self.setNavigationItem()
        
        captureSession.sessionPreset = AVCaptureSession.Preset.vga640x480
        
        let devices = AVCaptureDevice.devices()
        // Loop through all the capture devices on this phone
        for device in devices {
            // Make sure this particular device supports video
            if (device.hasMediaType(AVMediaType.video)) {
                // Finally check the position and confirm we've got the front camera
                if(device.position == AVCaptureDevice.Position.front) {
                    captureDevice = device
                }
            }
        }
        
        if captureDevice != nil {
            beginSession()
        }
        
    }
    
    //* cf.http://www.howtobuildsoftware.com/index.php/how-do/SB0/ios-swift-uiview-front-camera-to-fill-circular-uiview
    
    func beginSession() {
        do {
            // Get an instance of the AVCaptureDeviceInput class using the previous device object.
            let input = try AVCaptureDeviceInput(device: captureDevice!)
            
            // Set the input device on the capture session.
            captureSession.addInput(input)
            
        } catch {
            // If any error occurs, simply print it out and don't continue any more.
            print(error)
            return
        }
        
//        cameraView.frame = CGRectMake(10, 100, 300, 300)
        cameraView.backgroundColor = UIColor(red:26/255, green:188/255, blue:156/255, alpha:1)
        let tribuColor = UIColor(red:0/255, green:216/255, blue:254/255, alpha:1)
        cameraView.layer.cornerRadius = (cameraView.frame.size.width) / 2
        cameraView.layer.borderColor = tribuColor.cgColor
        cameraView.layer.borderWidth = 2
        cameraView.contentMode = UIViewContentMode.scaleToFill
        cameraView.layer.masksToBounds = true
        
        //view.addSubview(cameraView)
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravity.resizeAspectFill
        
        let rootLayer :CALayer = cameraView.layer
        rootLayer.masksToBounds=true
        videoPreviewLayer!.frame = rootLayer.bounds
        rootLayer.addSublayer(videoPreviewLayer!)
        // cf.iOS 10 Book p.836
        let outputSettings:NSDictionary =   NSDictionary(object: AVVideoCodecJPEG, forKey: AVVideoCodecKey as NSCopying)
        stillImageOutput.outputSettings =   outputSettings as! [String : Any]
        captureSession.addOutput(stillImageOutput)
        //
        captureSession.startRunning()
    }
    
    func CGRectMake(_ x: CGFloat, _ y: CGFloat, _ width: CGFloat, _ height: CGFloat) -> CGRect {
        return CGRect(x: x, y: y, width: width, height: height)
    }
    
    func resize(image: UIImage)->UIImage{
        var actualHeight:CGFloat            =   image.size.height
        var actualWidth:CGFloat             =   image.size.width
        
        var maxHeight:CGFloat               =   0.0//idScan?300.0:1224.0
        var maxWidth:CGFloat                =   0.0//idScan?400.0:1224.0
        
        if self.idCardScanning{maxHeight    =   300.0}else{maxHeight    =   1224.0}
        if self.idCardScanning{maxWidth     =   300.0}else{maxWidth     =   1224.0}
        
        var imgRatio:CGFloat                =   actualWidth/actualHeight;
        let maxRatio:CGFloat                =   maxWidth/maxHeight;
        var compressionQuality:CGFloat      =   0.0
        if self.idCardScanning{compressionQuality = 0.5}else{compressionQuality = 0.0}
        
        if (actualHeight > maxHeight || actualWidth > maxWidth)
        {
            if(imgRatio < maxRatio)
            {
                //adjust width according to maxHeight
                imgRatio = maxHeight / actualHeight;
                actualWidth = imgRatio * actualWidth;
                actualHeight = maxHeight;
            }
            else if(imgRatio > maxRatio)
            {
                //adjust height according to maxWidth
                imgRatio = maxWidth / actualWidth;
                actualHeight = imgRatio * actualHeight;
                actualWidth = maxWidth;
            }
            else
            {
                actualHeight = maxHeight;
                actualWidth = maxWidth;
            }
        }
        
        let rect:CGRect                     =   CGRect(x: 0.0, y: -80.0, width: actualWidth, height: actualHeight)
        UIGraphicsBeginImageContext(rect.size)
        image.draw(in: rect)
        let img                             =   UIGraphicsGetImageFromCurrentImageContext()
        let imageData                       =   UIImageJPEGRepresentation(img!, compressionQuality)
        
        UIGraphicsEndImageContext()
        
        return UIImage(data: imageData!)!
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        // This is the rect that we've calculated out and this is what is actually used below
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    // Identify a person
    func identifyPerson(personGroupId: String, image: UIImage) {
        self.apiface.detectFaces(image: image) { faceIdResult in
            let faceId = faceIdResult
            self.apiface.identifyPerson(faceIds: [faceId], personGroupId: personGroupId) { identifiedPersonResult in
                let identifiedPerson = identifiedPersonResult
                
                if identifiedPerson[0]["candidates"].isEmpty == false {
                    let personId = identifiedPerson[0]["candidates"][0]["personId"].string!
                    self.apiface.retrievePerson(personId: personId, personGroupId: personGroupId) { retrievedPerson in
                        print(retrievedPerson)
                        self.username.text = retrievedPerson["name"].string!
                        self.photoButton.image = UIImage(named: "fleche")
                        
                        self.registerButton.isHidden = true
                        self.forgottenPinButton.isHidden = true
                        
                        self.username.setNeedsDisplay()
                        self.photoButton.setNeedsDisplay()
                        self.cameraView.setNeedsDisplay()
                        self.registerButton.setNeedsDisplay()
                        self.forgottenPinButton.setNeedsDisplay()
                        
                        self.recognized = true
                    }
                } else {
                    print("User not found !")
                    self.username.text = "Utilisateur non reconnu"
                    self.username.setNeedsDisplay()
                    
                    self.recognized = false
                    
                    //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "USER_NOT_FOUND"), object: self.image)
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension UIViewController {
    func setNavigationItem() {
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "SplashIcon")
        imageView.image = image
        navigationItem.titleView = imageView
    }
}

