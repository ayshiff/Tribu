//
//  CellTableViewCell.swift
//  Tribu
//
//  Created by Rémi Doreau on 15/04/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class CellTableViewCell: UITableViewCell {

    @IBOutlet weak var cell: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCell(nameCell: String){
        self.cell.text = nameCell
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
