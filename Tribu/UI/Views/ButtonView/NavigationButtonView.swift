//
//
//  NavigationButtonView.swift
//  Tribu
//
//  Created by Rémi Doreau on 31/01/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class NavigationButtonView: UIButton {
    
    @IBOutlet var contentView: UIButton!
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    private func commonInit() {
        Bundle.main.loadNibNamed("NavigationButtonView", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.layer.cornerRadius = contentView.bounds.size.height/2
        contentView.autoresizingMask=[.flexibleHeight, .flexibleWidth]
    }
}

