//
//  CustomHeader.swift
//  Tribu
//
//  Created by Rémi Doreau on 31/01/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class CustomHeader: UINavigationBar {

    override func sizeThatFits(_ size: CGSize) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: 55)
    }

}
