//
//  MemberController.swift
//  Tribu
//
//  Created by Rémi Doreau on 14/02/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class MemberController: UIViewController {

    @IBOutlet weak var MemberCard: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        MemberCard.layer.cornerRadius = 10.0
//        MemberCard.layer.shadowColor = UIColor.gray.cgColor
//        MemberCard.layer.shadowOffset = CGSize.zero
//        MemberCard.layer.shadowOpacity = 0.2
//        MemberCard.layer.shadowRadius = 7.0
        MemberCard.layer.masksToBounds =  false
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
