//
//  EBRoundedTabBarController.swift
//  RoundedTabBarControllerExample
//
//  Created by Erid Bardhaj on 10/28/18.
//  Copyright © 2018 Erid Bardhaj. All rights reserved.
//

import UIKit

class EBRoundedTabBarController: UITabBarController {
    
    let containerView = UIView()
    
    let menuButton = UIButton(frame: CGRect(x: 0, y: 0, width: 64, height: 64))
    
    // MARK: - Inner Types
    
    private struct Constants {
        static let actionButtonSize = CGSize(width: 64, height: 64)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton = true
        self.setNavigationTabBarItem()
        
        setupSubviews()
        
        // Set up the view for buttons
//        containerView.frame = CGRect(x: 100, y: 100, width: 300, height: 240)
        view.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        // anchor view right above the tabBar
        containerView.bottomAnchor.constraint(equalTo: tabBar.topAnchor).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 80).isActive = true
        containerView.widthAnchor.constraint(equalToConstant: 180).isActive = true
        containerView.centerXAnchor.constraint(equalTo: tabBar.centerXAnchor).isActive = true
        containerView.alpha = 0.0
        
        // Left icon tab
        let image = UIImage(named: "plus_account")
        let imageView = UIImageView(image: image!)
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTapNewAccount(_:)))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(tap)
        
        imageView.frame = CGRect(x: 0, y: 25, width: 60, height: 60)
        containerView.addSubview(imageView)
        
        // Middle icon tab
        let image2 = UIImage(named: "plus_card")
        let imageView2 = UIImageView(image: image2!)
        
        imageView2.frame = CGRect(x: 60, y: 0, width: 60, height: 60)
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(self.handleTapNewCard(_:)))
        imageView2.isUserInteractionEnabled = true
        imageView2.addGestureRecognizer(tap2)
        containerView.addSubview(imageView2)
        
        // Right icon tab
        let image3 = UIImage(named: "plus_members")
        let imageView3 = UIImageView(image: image3!)
        
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(self.handleTapNewMember(_:)))
        imageView3.isUserInteractionEnabled = true
        imageView3.frame = CGRect(x: 120, y: 25, width: 60, height: 60)
        imageView3.addGestureRecognizer(tap3)
        containerView.addSubview(imageView3)
        
        containerView.isHidden = true
    }
    
    // Handle press action on tab
    @objc func handleTapNewAccount(_ sender: UITapGestureRecognizer) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "NewAccount")
        present(newViewController, animated: true, completion: nil)
    }
    
    @objc func handleTapNewMember(_ sender: UITapGestureRecognizer) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "NewMember")
        present(newViewController, animated: true, completion: nil)
    }
    
    @objc func handleTapNewCard(_ sender: UITapGestureRecognizer) {
//        let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "NewCard")
//        self.navigationController?.pushViewController(vc, animated: true)

        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "NewCard")
        present(newViewController, animated: true, completion: nil)
    }
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//
////        actionButton.isHidden = false
//    }
    
    
    // MARK: - Setups
    
    private func setupSubviews() {
        createTabbarControllers()
//        let imageView = UIImageView(image: actionButton)
//        view.addSubview(actionButton)
    }

    
    
    // MARK: - Actions
    
    @objc private func actionButtonTapped(sender: UIButton) {
        
    }
    
    
    // MARK: - Helpers
    
    private func createTabbarControllers() {
        setupMiddleButton()
        self.viewControllers = viewControllers
    }
    
    func setupMiddleButton() {
        var menuButtonFrame = menuButton.frame
        menuButtonFrame.origin.y = view.bounds.height - menuButtonFrame.height
        menuButtonFrame.origin.x = view.bounds.width/2 - menuButtonFrame.size.width/2
        menuButton.frame = menuButtonFrame
        
        menuButton.backgroundColor = UIColor.gray
        menuButton.layer.cornerRadius = menuButtonFrame.height/2
        view.addSubview(menuButton)
        
        menuButton.setImage(UIImage(named: "plus-big"), for: .normal)
        menuButton.addTarget(self, action: "showButtons:", for: .touchUpInside)
        
        view.layoutIfNeeded()
    }
    
    @objc func showButtons(_ sender: UITapGestureRecognizer) {
        if containerView.isHidden == true {
            UIView.animate(withDuration: 0.5, animations: {
                self.containerView.alpha = 1.0
                self.containerView.isHidden = false
                self.menuButton.setImage(UIImage(named: "plus-big-active"), for: .normal)
            })
        } else {
            UIView.animate(withDuration: 0.5, animations: {
                self.containerView.alpha = 0.0
                self.containerView.isHidden = true
                self.menuButton.setImage(UIImage(named: "plus-big"), for: .normal)
            })
        }
    }
    
    private func createController(for customTabBarItem: EBRoundedTabBarItem, with tag: Int) -> UINavigationController? {
        let viewController = UIViewController()
        viewController.title = customTabBarItem.title
        viewController.tabBarItem = customTabBarItem.tabBarItem
        
        return UINavigationController(rootViewController: viewController)
    }
}

extension UITabBarController {
    func setNavigationTabBarItem() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named:"interrogation-20"), style: .plain, target: self, action: #selector(buttonClicked))
        
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        imageView.contentMode = .scaleAspectFit
        let image = UIImage(named: "SplashIcon")
        imageView.image = image
        navigationItem.titleView = imageView
    }
    
    @objc func buttonClicked(_ sender: AnyObject?) {
        let popup : UIViewController = (self.storyboard?.instantiateViewController(withIdentifier: "ModalView"))!
        let navigationController = UINavigationController(rootViewController: popup)
//        navigationController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(navigationController, animated: true, completion: nil)
    }
}

