//
//  StepOneCardViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 05/04/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit
import WebKit

class StepOneCardViewController: UIViewController {

    @IBOutlet weak var webview: UIWebView!
    override func viewDidLoad() {
        super.viewDidLoad()
        let localPath = Bundle.main.url(forResource: "tribu_logo", withExtension: "html")
        let myRequest = URLRequest(url: localPath!)
        self.webview.scrollView.isScrollEnabled = false
        self.webview.loadRequest(myRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
