//
//  DefaultCardViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 01/04/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class DefaultCardViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set shadow to UIView
        self.view.subviews[0].layer.shadowPath =
            UIBezierPath(roundedRect: self.view.subviews[0].bounds,
                         cornerRadius: self.view.subviews[0].layer.cornerRadius).cgPath
        self.view.subviews[0].layer.shadowColor = UIColor.black.cgColor
        self.view.subviews[0].layer.shadowOpacity = 0.1
        self.view.subviews[0].layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        self.view.subviews[0].layer.shadowRadius = 10
        self.view.subviews[0].layer.masksToBounds = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
