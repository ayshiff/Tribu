//
//  CardController.swift
//  Tribu
//
//  Created by Rémi Doreau on 14/02/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class CardController: UIViewController {

    @IBOutlet weak var CardComponent: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        CardComponent.layer.cornerRadius = 10.0
//        CardComponent.layer.shadowColor = UIColor.gray.cgColor
//        CardComponent.layer.shadowOffset = CGSize.zero
//        CardComponent.layer.shadowOpacity = 0.2
//        CardComponent.layer.shadowRadius = 7.0
        CardComponent.layer.masksToBounds =  false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
