//
//  HomeController.swift
//  Tribu
//
//  Created by Rémi Doreau on 11/02/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class HomeController: UICollectionViewController {
    
    @IBOutlet var myCollectionView: UICollectionView!
    var Array = [String]()
    
    var ImageArray = [String]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Array = ["Mes comptes", "Mes CB", "Mes authentifications", "Ma Tribu", "Mes RIB", "Mes virements"]
        
        ImageArray = ["account_dashboard", "cards_dashboard", "authentification_dashboard", "members_dashboard", "rib_dashboard", "arrow_dashboard"]
        
        let ItemSize = UIScreen.main.bounds.width/2.3 - 2.3
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsetsMake(20,20,20,20)
        layout.itemSize = CGSize(width: ItemSize , height: ItemSize)
        
        layout.minimumInteritemSpacing = 15
        layout.minimumLineSpacing = 15
        
        myCollectionView.collectionViewLayout = layout

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Register cell classes
//        self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }
    */

    // MARK: UICollectionViewDataSource

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    @IBAction func pushNewController(_ sender: UITapGestureRecognizer) {
        
        let yourVC = self.storyboard?.instantiateViewController(withIdentifier: "Account")
        let navController = UINavigationController(rootViewController: yourVC!)
//        navController.modalPresentationStyle = .overCurrentContext
        self.present(navController, animated: true, completion: nil)
        
//        let next = self.storyboard?.instantiateViewController(withIdentifier: "Account")
//        self.present(next!, animated: true, completion: nil)
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return Array.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        let Button = cell.viewWithTag(1) as! UILabel
        
        let Image = cell.viewWithTag(2) as! UIImageView
        
//        cell.layer.cornerRadius = 3
//        cell.layer.shadowColor = UIColor.black.cgColor
//        cell.layer.shadowOffset = CGSize(width: 0, height: 2.0)
//        cell.layer.shadowRadius = 2.0
//        cell.layer.shadowOpacity = 0.5
        
        Button.text = Array[indexPath.row]
        
        Image.image = UIImage(named: ImageArray[indexPath.item])
    
        // Configure the cell
        cell.layer.cornerRadius = 10
        cell.layer.shadowColor = UIColor.lightGray.cgColor
        cell.layer.shadowOffset = CGSize(width:0,height: 3.0)
        cell.layer.shadowRadius = 10.0
        cell.layer.shadowOpacity = 0.3
        cell.layer.masksToBounds = false;
        cell.layer.shadowPath = UIBezierPath(roundedRect:cell.bounds, cornerRadius:cell.contentView.layer.cornerRadius).cgPath
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(10, 4, 10, 4)
    }

    // MARK: UICollectionViewDelegate

    /*
    // Uncomment this method to specify if the specified item should be highlighted during tracking
    override func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment this method to specify if the specified item should be selected
    override func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
    */

    /*
    // Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        return false
    }

    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
    
    }
    */

}
