//
//  TribuPurchasesController.swift
//  Tribu
//
//  Created by Rémi Doreau on 25/03/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit
import FirebaseDatabase

class TribuPurchasesController: UIViewController {
    @IBOutlet weak var amountPurchases: UILabel!
    var ref: DatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.ref = Database.database().reference()
        
        self.ref.child("achat").observe(.value, with: { (snapshot) in
            
            let value = snapshot.value as! Int
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "AmountPurchase"), object: value)
            let textSplittedPurchase = self.amountPurchases.text?.components(separatedBy: " ")
            self.amountPurchases.text = String(Int(textSplittedPurchase![0])! + value) + " " + textSplittedPurchase![1]
            
        }) { (error) in
            print(error)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
