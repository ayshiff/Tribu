//
//  CardSoldController.swift
//  Tribu
//
//  Created by Rémi Doreau on 25/03/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class CardSoldController: UIViewController {

    @IBOutlet weak var soldCardAmout: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: NSNotification.Name(rawValue: "AmountPurchase"), object: nil)
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        let result = notification.object! as! Int
        
        let textSplittedPurchase = self.soldCardAmout.text?.components(separatedBy: " ")
        self.soldCardAmout.text = textSplittedPurchase![0] + " " + String(Int(textSplittedPurchase![1])! - result) + " " + textSplittedPurchase![2]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
