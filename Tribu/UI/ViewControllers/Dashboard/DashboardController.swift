//
//  DashboardController.swift
//  Tribu
//
//  Created by Rémi Doreau on 14/02/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class DashboardController: UIViewController {
    @IBOutlet weak var soldeAmount: UILabel!
    @IBOutlet weak var CardSet: UIView!
    @IBOutlet weak var TribuPurchase: UIView!
    
    
    @IBOutlet weak var view1: UIView!
    @IBOutlet weak var view2: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: NSNotification.Name(rawValue: "AmountPurchase"), object: nil)
        
        let tapGesture1 = UITapGestureRecognizer(target: self, action: #selector(handleTap1(sender:)))
        
        let tapGesture2 = UITapGestureRecognizer(target: self, action: #selector(handleTap2(sender:)))
        
        view1.addGestureRecognizer(tapGesture1)
        view2.addGestureRecognizer(tapGesture2)
        
        // Set shadow to CardSet
        self.CardSet.layer.shadowPath =
            UIBezierPath(roundedRect: self.CardSet.bounds,
                         cornerRadius: self.CardSet.layer.cornerRadius).cgPath
        self.CardSet.layer.shadowColor = UIColor.black.cgColor
        self.CardSet.layer.shadowOpacity = 0.1
        self.CardSet.layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        self.CardSet.layer.shadowRadius = 10
        self.CardSet.layer.masksToBounds = false
        
        // Set shadow to TribuPurchase
        self.TribuPurchase.layer.shadowPath =
            UIBezierPath(roundedRect: self.TribuPurchase.bounds,
                         cornerRadius: self.TribuPurchase.layer.cornerRadius).cgPath
        self.TribuPurchase.layer.shadowColor = UIColor.black.cgColor
        self.TribuPurchase.layer.shadowOpacity = 0.1
        self.TribuPurchase.layer.shadowOffset = CGSize(width: -0.5, height: 0.5)
        self.TribuPurchase.layer.shadowRadius = 10
        self.TribuPurchase.layer.masksToBounds = false
    }
    
    @objc func handleTap1(sender: UITapGestureRecognizer) {
        self.tabBarController?.selectedIndex = 3
    }
    
    @objc func handleTap2(sender: UITapGestureRecognizer) {
        self.tabBarController?.selectedIndex = 4
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        let result = notification.object! as! Int
        
        let textSplittedPurchase = self.soldeAmount.text?.components(separatedBy: " ")
        self.soldeAmount.text = textSplittedPurchase![0] + " " + String(Int(textSplittedPurchase![1])! - result) + " " + textSplittedPurchase![2]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
