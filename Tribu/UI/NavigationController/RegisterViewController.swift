//
//  RegisterViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 31/01/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nextStepButton: UIImageView!
    
    var registerPageViewController: RegisterPageViewController? {
        didSet {
            registerPageViewController?.registerDelegate = self as? RegisterPageViewControllerDelegate
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       self.setNavigationItem()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: Notification.Name("successRegister"), object: nil)
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        registerPageViewController?.scrollToNextViewController()
        nextStepButton.isHidden = false
        nextStepButton.image = UIImage(named: "fleche")
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let registerPageViewController = segue.destination as? RegisterPageViewController {
            self.registerPageViewController = registerPageViewController
        }
    }
    @IBAction func didTapNextButton(_ sender: UITapGestureRecognizer) {
        let t = registerPageViewController?.scrollToNextViewController()
        if (t == 2) {
            nextStepButton.isHidden = true
        } else {
            nextStepButton.image = UIImage(named: "fleche")
        }
    }
    
    
    /**
     Fired when the user taps on the pageControl to change its current page.
     */
}
