//
//  RegisterSignatureController.swift
//  Tribu
//
//  Created by Rémi Doreau on 20/02/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class RegisterSignatureController: UIViewController {

    @IBOutlet weak var canvasView: CanvasView!
    @IBAction func clearAction(_ sender: UIButton) {
        canvasView.clearCanvas()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
