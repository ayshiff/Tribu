//
//  NewAccountViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 05/03/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class NewAccountViewController: UIViewController {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    
    var newAccountPageViewController: NewAccountPageViewController? {
        didSet {
            newAccountPageViewController?.newAccountDelegate = self
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationItem()

        pageControl.addTarget(self, action: Selector("didChangePageControlValue"), for: .valueChanged)
    }
    
    @IBAction func closeAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let newAccountPageViewController = segue.destination as? NewAccountPageViewController {
            self.newAccountPageViewController = newAccountPageViewController
        }
    }
    @IBAction func didTapNextButton(_ sender: UITapGestureRecognizer) {
        newAccountPageViewController?.scrollToNextViewController()
    }
    
    /**
     Fired when the user taps on the pageControl to change its current page.
     */
    func didChangePageControlValue() {
        newAccountPageViewController?.scrollToViewController(index: pageControl.currentPage)
    }
}

extension NewAccountViewController: NewAccountPageViewControllerDelegate {
    
    func newAccountPageViewController(newAccountPageViewController: NewAccountPageViewController,
                                   didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func newAccountPageViewController(newAccountPageViewController: NewAccountPageViewController,
                                   didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
    
}
