//
//  NewAccountPageViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 05/03/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class NewAccountPageViewController: UIPageViewController {
    
    weak var newAccountDelegate: NewAccountPageViewControllerDelegate?
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        // The view controllers will be shown in this order
        return [
            self.newColoredViewController("1"),
            self.newColoredViewController("2"),
            ]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self

        if let initialViewController = orderedViewControllers.first {
            scrollToViewController(viewController: initialViewController)
        }
        
        newAccountDelegate?.newAccountPageViewController(newAccountPageViewController: self, didUpdatePageCount: orderedViewControllers.count)
    }
    
    /**
     Scrolls to the next view controller.
     */
    func scrollToNextViewController() {
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self, viewControllerAfter: visibleViewController) {
            if visibleViewController == orderedViewControllers.last {
                navigationController?.popViewController(animated: true)
                dismiss(animated: true, completion: nil)
                
            } else {
                scrollToViewController(viewController: nextViewController)
            }
        }
    }
    
    /**
     Scrolls to the view controller at the given index. Automatically calculates
     the direction.
     
     - parameter newIndex: the new index to scroll to
     */
    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedViewControllers.index(of: firstViewController) {
            //            let direction: UIPageViewController.NavigationDirection = newIndex >= currentIndex ? .forward : .reverse
            let nextViewController = orderedViewControllers[newIndex]
            scrollToViewController(viewController: nextViewController)
        }
    }
    
    func newColoredViewController(_ color: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "NewAccount\(color)")
    }
    
    /**
     Scrolls to the given 'viewController' page.
     
     - parameter viewController: the view controller to show.
     */
    private func scrollToViewController(viewController: UIViewController,
                                        direction: UIPageViewControllerNavigationDirection = .forward) {
        setViewControllers([viewController],
                           direction: direction,
                           animated: true,
                           completion: { (finished) -> Void in
                            // Setting the view controller programmatically does not fire
                            // any delegate methods, so we have to manually notify the
                            // 'newAccountDelegate' of the new index.
                            self.notifyNewAccountDelegateOfNewIndex()
        })
    }
    
    /**
     Notifies '_newAccountDelegate' that the current page index was updated.
     */
    func notifyNewAccountDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first,
            let index = orderedViewControllers.index(of: firstViewController) {
            newAccountDelegate?.newAccountPageViewController(newAccountPageViewController: self, didUpdatePageIndex: index)
        }
    }
    
}

// MARK: UIPageViewControllerDataSource

extension NewAccountPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        // User is on the first view controller and swiped left to loop to
        // the last view controller.
        guard previousIndex >= 0 else {
            return orderedViewControllers.last
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        // User is on the last view controller and swiped right to loop to
        // the first view controller.
        guard orderedViewControllersCount != nextIndex else {
            return orderedViewControllers.first
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return orderedViewControllers[nextIndex]
    }
    
}

extension NewAccountPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController,
                            didFinishAnimating finished: Bool,
                            previousViewControllers: [UIViewController],
                            transitionCompleted completed: Bool) {
        notifyNewAccountDelegateOfNewIndex()
    }
    
}

protocol NewAccountPageViewControllerDelegate: class {
    
    /**
     Called when the number of pages is updated.
     
     - parameter newAccountPageViewController: the NewAccountPageViewController instance
     - parameter count: the total number of pages.
     */
    func newAccountPageViewController(newAccountPageViewController: NewAccountPageViewController,
                                   didUpdatePageCount count: Int)
    
    /**
     Called when the current index is updated.
     
     - parameter newAccountPageViewController: the NewAccountPageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func newAccountPageViewController(newAccountPageViewController: NewAccountPageViewController,
                                   didUpdatePageIndex index: Int)
    
}
