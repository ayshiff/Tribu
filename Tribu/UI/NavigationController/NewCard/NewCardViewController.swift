//
//  NewCardViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 31/01/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class NewCardViewController: UIViewController {

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    
    var newCardPageViewController: NewCardPageViewController? {
        didSet {
            newCardPageViewController?.newCardDelegate = self
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationItem()
        pageControl.addTarget(self, action: Selector("didChangePageControlValue"), for: .valueChanged)
    }
    @IBAction func closeAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let newCardPageViewController = segue.destination as? NewCardPageViewController {
            self.newCardPageViewController = newCardPageViewController
        }
    }
    @IBAction func didTapNextButton(_ sender: UITapGestureRecognizer) {
        newCardPageViewController?.scrollToNextViewController()
    }
    
    
    /**
     Fired when the user taps on the pageControl to change its current page.
     */
    func didChangePageControlValue() {
        newCardPageViewController?.scrollToViewController(index: pageControl.currentPage)
    }
}

extension NewCardViewController: NewCardPageViewControllerDelegate {
    
    func newCardPageViewController(newCardPageViewController: NewCardPageViewController,
        didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func newCardPageViewController(newCardPageViewController: NewCardPageViewController,
        didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
    
}
