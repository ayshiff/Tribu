//
//  RegisterPageViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 31/01/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class RegisterPageViewController: UIPageViewController {
    
    weak var registerDelegate: RegisterPageViewControllerDelegate?
    
    private(set) lazy var orderedViewControllers: [UIViewController] = {
        // The view controllers will be shown in this order
        return [
            self.newColoredViewController("1"),
            self.newColoredViewController("2"),
            self.newColoredViewController("3"),
            self.newColoredViewController("4"),
            self.newColoredViewController("5"),
        ]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate = self
        
        if let initialViewController = orderedViewControllers.first {
            scrollToViewController(viewController: initialViewController)
        }
        
        registerDelegate?.registerPageViewController(registerPageViewController: self, didUpdatePageCount: orderedViewControllers.count)
    }

    /**
     Scrolls to the next view controller.
     */
    func scrollToNextViewController() -> intmax_t {
        if let visibleViewController = viewControllers?.first,
            let nextViewController = pageViewController(self, viewControllerAfter: visibleViewController) {
            if visibleViewController == orderedViewControllers.last {
                let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let newViewController = storyBoard.instantiateViewController(withIdentifier: "HomeContainer")
                self.navigationController?.pushViewController(newViewController, animated: true)
            } else {
                scrollToViewController(viewController: nextViewController)
            }
        }
        let indexPage = pageViewController(self, viewControllerAfter: (viewControllers?.first)!)
        return orderedViewControllers.index(of: indexPage!)!
    }
    
    /**
     Scrolls to the view controller at the given index. Automatically calculates
     the direction.
     
     - parameter newIndex: the new index to scroll to
     */
    func scrollToViewController(index newIndex: Int) {
        if let firstViewController = viewControllers?.first,
            let currentIndex = orderedViewControllers.index(of: firstViewController) {
//            let direction: UIPageViewController.NavigationDirection = newIndex >= currentIndex ? .forward : .reverse
                let nextViewController = orderedViewControllers[newIndex]
                scrollToViewController(viewController: nextViewController)
        }
    }
    
    func newColoredViewController(_ color: String) -> UIViewController {
        return UIStoryboard(name: "Main", bundle: nil) .
            instantiateViewController(withIdentifier: "Register\(color)")
    }
    
    /**
     Scrolls to the given 'viewController' page.
     
     - parameter viewController: the view controller to show.
     */
    private func scrollToViewController(viewController: UIViewController,
                                        direction: UIPageViewControllerNavigationDirection = .forward) {
        setViewControllers([viewController],
            direction: direction,
            animated: true,
            completion: { (finished) -> Void in
                // Setting the view controller programmatically does not fire
                // any delegate methods, so we have to manually notify the
                // 'registerDelegate' of the new index.
                self.notifyRegisterDelegateOfNewIndex()
        })
    }
    
    /**
     Notifies '_registerDelegate' that the current page index was updated.
     */
    func notifyRegisterDelegateOfNewIndex() {
        if let firstViewController = viewControllers?.first,
            let index = orderedViewControllers.index(of: firstViewController) {
                registerDelegate?.registerPageViewController(registerPageViewController: self, didUpdatePageIndex: index)
        }
    }
    
}

// MARK: UIPageViewControllerDataSource

extension RegisterPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
                return nil
            }
            
            let previousIndex = viewControllerIndex - 1
            
            // User is on the first view controller and swiped left to loop to
            // the last view controller.
            guard previousIndex >= 0 else {
                return orderedViewControllers.last
            }
            
            guard orderedViewControllers.count > previousIndex else {
                return nil
            }
            
            return orderedViewControllers[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
            guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
                return nil
            }
            
            let nextIndex = viewControllerIndex + 1
            let orderedViewControllersCount = orderedViewControllers.count
            
            // User is on the last view controller and swiped right to loop to
            // the first view controller.
            guard orderedViewControllersCount != nextIndex else {
                return orderedViewControllers.first
            }
            
            guard orderedViewControllersCount > nextIndex else {
                return nil
            }
            
            return orderedViewControllers[nextIndex]
    }
    
}

extension RegisterPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(pageViewController: UIPageViewController,
        didFinishAnimating finished: Bool,
        previousViewControllers: [UIViewController],
        transitionCompleted completed: Bool) {
        notifyRegisterDelegateOfNewIndex()
    }
    
}

protocol RegisterPageViewControllerDelegate: class {
    
    /**
     Called when the number of pages is updated.
     
     - parameter registerPageViewController: the RegisterPageViewController instance
     - parameter count: the total number of pages.
     */
    func registerPageViewController(registerPageViewController: RegisterPageViewController,
        didUpdatePageCount count: Int)
    
    /**
     Called when the current index is updated.
     
     - parameter registerPageViewController: the RegisterPageViewController instance
     - parameter index: the index of the currently visible page.
     */
    func registerPageViewController(registerPageViewController: RegisterPageViewController,
        didUpdatePageIndex index: Int)
    
}
