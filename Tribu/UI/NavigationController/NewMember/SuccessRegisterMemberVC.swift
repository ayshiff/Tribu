//
//  SucessRegisterViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 12/03/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class SucessRegisterMemberVC: UIViewController {
    
    @IBOutlet weak var personName: UILabel!
    @IBOutlet weak var personPhoto: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let name = UserDefaults.standard.string(forKey: "MemberName")
        let surname = UserDefaults.standard.string(forKey: "MemberSurname")
        
        let completeName = name! + " " + surname!
        
        // Set user name from NSUserDefaults
        self.personName.text = "Photo de " + completeName
        
        // Set user photo from NSUserDefaults
        let imgData = UserDefaults.standard.value(forKey: "MemberPhoto") as? Data
        
        if let image = UIImage(data: imgData!)
        {
            //set image in UIImageView imgSignature
            self.personPhoto.image = image
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
