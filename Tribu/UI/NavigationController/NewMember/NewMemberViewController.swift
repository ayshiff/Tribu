//
//  NewMember.swift
//  Tribu
//
//  Created by Rémi Doreau on 05/03/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

import UIKit

class NewMemberViewController: UIViewController {
    
    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nextStepButton: UIImageView!
    
    var newMemberPageViewController: NewMemberPageViewController? {
        didSet {
            newMemberPageViewController?.newMemberDelegate = self
        }
    }
    
    @IBAction func closeAction(_ sender: Any) {
        navigationController?.popViewController(animated: true)
        
        dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationItem()
        pageControl.addTarget(self, action: Selector("didChangePageControlValue"), for: .valueChanged)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onDidReceiveData(_:)), name: Notification.Name("successRegisterMember"), object: nil)
    }
    
    @objc func onDidReceiveData(_ notification:Notification) {
        newMemberPageViewController?.scrollToNextViewController()
        nextStepButton.isHidden = false
        nextStepButton.image = UIImage(named: "fleche")
    }
    
    @IBAction func closeModal(_ sender: UITapGestureRecognizer) {
        print("close modal")
//        navigationController?.popViewController(animated: true)
//        dismiss(animated: true, completion: nil)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let newMemberPageViewController = segue.destination as? NewMemberPageViewController {
            self.newMemberPageViewController = newMemberPageViewController
        }
    }
    @IBAction func didTapNextButton(_ sender: UITapGestureRecognizer) {
        let t = newMemberPageViewController?.scrollToNextViewController()
        if (t == 3) {
            nextStepButton.isHidden = true
        } else {
            nextStepButton.image = UIImage(named: "fleche")
        }
    }
    
    
    /**
     Fired when the user taps on the pageControl to change its current page.
     */
    func didChangePageControlValue() {
        newMemberPageViewController?.scrollToViewController(index: pageControl.currentPage)
    }
}

extension NewMemberViewController: NewMemberPageViewControllerDelegate {
    
    func newMemberPageViewController(newMemberPageViewController: NewMemberPageViewController,
                                      didUpdatePageCount count: Int) {
        pageControl.numberOfPages = count
    }
    
    func newMemberPageViewController(newMemberPageViewController: NewMemberPageViewController,
                                      didUpdatePageIndex index: Int) {
        pageControl.currentPage = index
    }
    
}
