//
//  AccountTabAfterViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 15/04/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class AccountTabAfterViewController: UIViewController {

    @IBOutlet weak var cellValue: UILabel!
    
    var cellule = String()
    
    @IBAction func closeModal(_ sender: Any) {
        let viewControllers = navigationController?.viewControllers
        navigationController?.popToViewController(viewControllers![(viewControllers?.count)! - 1], animated: true)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        cellValue.text = cellule
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
