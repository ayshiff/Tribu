//
//  AccountTableViewController.swift
//  Tribu
//
//  Created by Rémi Doreau on 15/04/2019.
//  Copyright © 2019 BRED. All rights reserved.
//

import UIKit

class AccountTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var tableau_cell = ["Axa Banque", "American Express", "Bred", "Test"]
    
    var cell_Selected = String()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableau_cell.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: CellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "Cell") as! CellTableViewCell
        let cellule = tableau_cell[indexPath.row]
        
        cell.setCell(nameCell: cellule)
        return cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.cell_Selected = tableau_cell[indexPath.row]
        self.performSegue(withIdentifier: "Segue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Segue" {
            let secondViewController = segue.destination as! AccountTabAfterViewController
            secondViewController.cellule = self.cell_Selected
        }
    }

}
